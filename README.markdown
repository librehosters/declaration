
# Declaration of libre hosting

- [Declaration of libre hosting](#declaration-of-libre-hosting)
    - [Signatories](#signatories)
    - [Legend](#legend)
        - [Editorial remarks](#editorial-remarks)
- [License](#license)

## Signatories

This document is undersigned by the following groups and persons:

for Ecobytes e.V., Witzenhausen/Hackenow, Jon Richter

```
Invitee list as of https://framagit.org/librehosters/declaration/issues/2#note_284495
```

## Legend

### Editorial remarks

We are embeddeing editorial remarks as

```
fenced boxes set in a monospace font.
```

# License

This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/.